#This script is for a simple number guessing game
print("Welcome to 'Guess that number!' by Alice Jane")
play=1
while play == 1:
    from random import randrange
    answer = randrange(101)
    solved = 0
    guess = input("I am thinking of a number between 0 and 100, what is it? ")
    guess_int = int(guess)
    attempts = 1
    if guess_int > answer:
        print("Lower!")
    if guess_int < answer:
        print("Higher!")
    if guess_int == answer:
        print("Winner winner chicken dinner!")
        print("You got it in one!")
        solved = 1
    while solved == 0:
        guess = input("Guess again! ")
        guess_int = int(guess)
        attempts = attempts +1
        if guess_int > answer:
            print("Lower!")
        if guess_int < answer:
            print("Higher!")
        if guess_int == answer:
            print("Winner winner chicken dinner!")
            print("Your score is",attempts)
            solved = 1
    playinput = raw_input("Would you like to play again? y/n: ")
    if playinput == "n":
        play = 0
